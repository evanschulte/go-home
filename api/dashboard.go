package api

import (
	"github.com/rs/zerolog/log"
	"go-home/pkg/config"
	"html/template"
	"net/http"
	"path"
)

type PageData struct {
	Title string
	Body  template.HTML
}

var tmpl *template.Template

func serveDashboard(w http.ResponseWriter, r *http.Request) {
	var conf = config.Load()
	data := PageData{
		Title: "Dashboard",
	}
	if tmpl == nil {
		tmpl = template.Must(template.ParseFiles(path.Join(conf.WebRoot, "index.html")))
	}
	err := tmpl.Execute(w, data)
	if err != nil {
		log.Error().Msgf("failed to render index template - %s", err)
		return
	}
}
