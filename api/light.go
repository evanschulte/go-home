package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	h "go-home/pkg/http"
	"go-home/pkg/light"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

func getLights(w http.ResponseWriter, r *http.Request) {
	lights, err := light.FetchAll()
	if err != nil {
		http.Error(w, "failed to get lights", http.StatusInternalServerError)
		return
	}

	h.Ok(w, lights)
}

func getLight(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil || id.IsZero() {
		http.Error(w, "request endpoint must contain a valid light id", http.StatusBadRequest)
		return
	}

	lt, err := light.Fetch(id)
	if err != nil {
		http.Error(w, "unable to find a light with the provided id", http.StatusNotFound)
		return
	}

	h.Ok(w, lt)
}

func addLight(w http.ResponseWriter, r *http.Request) {
	var lt *light.Light
	err := json.NewDecoder(r.Body).Decode(&lt)
	if err != nil {
		http.Error(w, "request body contain a valid light json object", http.StatusBadRequest)
		return
	} else if len(lt.IP) == 0 {
		http.Error(w, "request body must contain a valid ip address", http.StatusBadRequest)
		return
	} else if lt.Port <= 0 {
		http.Error(w, "request body must contain a valid port", http.StatusBadRequest)
		return
	}

	err = lt.Save()
	if err != nil {
		http.Error(w, "failed to save light to database", http.StatusInternalServerError)
		return
	}

	h.Ok(w, lt)
}

func updateLight(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil || id.IsZero() {
		http.Error(w, "request endpoint must contain a valid light id", http.StatusBadRequest)
		return
	}

	var lt *light.Light
	err = json.NewDecoder(r.Body).Decode(&lt)
	if err != nil {
		http.Error(w, "request body contain a valid light json object", http.StatusBadRequest)
		return
	} else if len(lt.IP) == 0 {
		http.Error(w, "request body must contain a valid ip address", http.StatusBadRequest)
		return
	} else if lt.Port <= 0 {
		http.Error(w, "request body must contain a valid port", http.StatusBadRequest)
		return
	}

	// create a new light struct and populate it from the db (if exists)
	ltC := light.Light{
		ID: id,
	}
	err = ltC.Fetch()
	if err != nil {
		http.Error(w, "unable to find a light with the provided id", http.StatusNotFound)
		return
	}
	ltC.IP = lt.IP
	ltC.Port = lt.Port
	err = ltC.Save()
	if err != nil {
		http.Error(w, "failed to save light to database", http.StatusInternalServerError)
		return
	}

	h.Ok(w, ltC)
}

func updateLightState(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil || id.IsZero() {
		http.Error(w, "request endpoint must contain a valid light id", http.StatusBadRequest)
		return
	}

	// parse the light to get the id, timer, and status
	var lt *light.Light
	err = json.NewDecoder(r.Body).Decode(&lt)
	if err != nil {
		http.Error(w, "request body contain a valid light json object", http.StatusBadRequest)
		return
	}

	// create a new light struct and populate it from the db (if exists)
	ltC := light.Light{
		ID: id,
	}
	err = ltC.Fetch()
	if err != nil {
		http.Error(w, "unable to find a light with the provided id", http.StatusNotFound)
		return
	}

	// set the timer and status of the db populated struct and push the state to the light
	ltC.Timer = lt.Timer
	ltC.Status = lt.Status
	ltC.Override = lt.Override
	err = ltC.PushState()
	if err != nil {
		http.Error(w, "failed to push new state to light", http.StatusInternalServerError)
		return
	}

	h.Ok(w, ltC)
}

func notify(w http.ResponseWriter, r *http.Request) {
	var lt *light.Light
	err := json.NewDecoder(r.Body).Decode(&lt)
	if err != nil {
		log.Trace().Err(err).Msg("failed to decode light from notify request")
		http.Error(w, "request body contain a valid light json object", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)

	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil || id.IsZero() {
		log.Trace().Err(err).Msg("notify request contained invalid id")
		http.Error(w, "request endpoint must contain a valid light id", http.StatusBadRequest)
		return
	}
	lt.ID = id

	ltC, err := light.Fetch(id)
	if err != nil {
		// TODO: maybe auto-add this light
		// although if its contacting this server and doesnt exist
		// its likely because it was deleted but the light controller is still on
		log.Trace().Err(err).Msg("notify request contained id that could not be found")
		http.Error(w, "unable to find a light with the provided id", http.StatusNotFound)
		return
	}

	// update state values sent from the light
	err = ltC.Notified(lt.Status, lt.Timer, lt.Override)
	if err != nil {
		http.Error(w, "failed to save device state", http.StatusInternalServerError)
		return
	}

	log.Trace().Msgf("light state change notification processed => %+v", ltC)

	h.Ok(w, ltC)
}
