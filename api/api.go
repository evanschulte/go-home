package api

import (
	"github.com/gorilla/mux"
	_ "github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"go-home/pkg/config"
	ws "go-home/pkg/websocket"
	"net/http"
	"net/http/pprof"
	"strconv"
	"time"
)

func Start() error {
	var conf = config.Load()
	log.Info().Msgf("server starting on port %d", conf.Port)

	r := mux.NewRouter()

	// root endpoint (website)
	r.HandleFunc("/", serveDashboard)

	// static assets
	r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir(conf.WebRoot))))

	// websocket endpoint
	var wsServer = ws.GetServer()
	r.HandleFunc("/connect", wsServer.Connect)

	// api subrouter
	api := r.PathPrefix("/api").Subrouter()

	// light endpoints
	lights := api.PathPrefix("/lights").Subrouter()
	lights.HandleFunc("", addLight).Methods("POST")
	lights.HandleFunc("", getLights).Methods("GET")
	lights.HandleFunc("/{id}", getLight).Methods("GET")
	lights.HandleFunc("/{id}", updateLightState).Methods("PATCH")
	lights.HandleFunc("/{id}", updateLight).Methods("PUT")
	lights.HandleFunc("/{id}/notify", notify).Methods("POST")

	// performance endpoints
	r.HandleFunc("/debug/pprof/", pprof.Index)
	r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	r.HandleFunc("/debug/pprof/profile", pprof.Profile)
	r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	r.HandleFunc("/debug/pprof/trace", pprof.Trace)
	r.Handle("/debug/pprof/heap", pprof.Handler("heap"))

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	})

	// health check endpoint
	r.HandleFunc("/teapot", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusTeapot)
	}).Methods("GET")

	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:" + strconv.Itoa(conf.Port),
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  30 * time.Second,
	}

	err := srv.ListenAndServe()
	if err != nil {
		log.Fatal().Msgf("failed to start http api on port %d", conf.Port)
		return err
	}

	log.Info().Msgf("api started on port %d", conf.Port)

	return nil
}
