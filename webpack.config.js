const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: './web/src/index.js',
    output: {
        path: __dirname + '/web/dist',
        publicPath: '/assets/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
                test: /\.(scss)$/,
                use: [{
                    loader: 'style-loader', // inject CSS to page
                }, {
                    loader: 'css-loader', // translates CSS into CommonJS modules
                }, {
                    loader: 'postcss-loader', // Run postcss actions
                    options: {
                        postcssOptions: {
                            plugins: [
                                require.resolve('autoprefixer')
                            ]
                        }
                    }
                }, {
                    loader: 'sass-loader' // compiles Sass to CSS
                }]
            },
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./web/src/index.html",
            filename: "./index.html"
        })
    ]
};