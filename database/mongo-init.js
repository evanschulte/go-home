db.auth('admin', 'admin');

db = db.getSiblingDB('gohome');

db.createUser({
    user: 'gohome',
    pwd: 'gohome',
    roles: [
        {
            role: 'root',
            db: 'admin',
        },
    ],
});