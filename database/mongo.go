package database

import (
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"go-home/pkg/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type MongoStore struct {
	Client *mongo.Client
	DB     *mongo.Database
}

var store MongoStore

func GetStore() *MongoStore {
	return &store
}

func Init() (*MongoStore, error) {
	conf := config.Load()
	store = MongoStore{}

	ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
	defer cancel()

	uri := fmt.Sprintf("mongodb://%s:%s@%s:%d/?authSource=%s", conf.DBUser, conf.DBPassword, conf.DBAddress, conf.DBPort, conf.DBName)
	var err error
	store.Client, err = mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal().Err(err).Msg("failed to create mongodb client")
		return nil, err
	}

	err = store.Client.Connect(ctx)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to mongodb")
		return nil, err
	}

	store.DB = store.Client.Database(conf.DBName)

	log.Info().Msgf("connected to mongodb => %s", uri)

	return &store, nil
}

func (s *MongoStore) Disconnect() error {
	if s.Client != nil {
		return s.Client.Disconnect(context.TODO())
	}
	log.Info().Msg("disconnected from mongodb")
	return nil
}
