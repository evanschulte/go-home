import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import LightPanel from "./components/light-panel";

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<LightPanel/>, wrapper) : false;