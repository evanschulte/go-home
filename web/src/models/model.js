import axios from 'axios';

export default class Model {
    idAttribute = "id";
    props = {};
    fetched = false;

    constructor(props) {
        this.props = props ? props : {};
    }

    isNew() {
        return !this.id();
    }

    id() {
        if (this.idAttribute && this.props) {
            return this.props[this.idAttribute];
        }
        return null;
    }

    mergeProps(props) {
        if (props) {
            this.props = {...this.props, ...props};
        }
    }

    url() {
        return null;
    }

    isFetched() {
        return this.fetched;
    }

    fetch(options = null) {
        return this.sync(null, {method: 'get', ...options}).then(res => {
            this.fetched = true;
            return res;
        });
    }

    patch(props = null, options = null) {
        this.mergeProps(props);
        return this.sync(props ? props : this.props, {method: 'patch', ...options});
    }

    put(props = null, options = null) {
        this.mergeProps(props);
        return this.sync(this.props, {method: 'put', ...options});
    }

    post(props = null, options = null) {
        this.mergeProps(props);
        return this.sync(this.props, {method: 'post', ...options});
    }

    save(props = null, options = null) {
        this.mergeProps(props);
        return this.sync(this.props, {method: this.isNew() ? 'post' : 'put', ...options});
    }

    sync(props, options = null) {
        let url = this.url();
        if (!url) {
            return new Promise(function (resolve) {
                resolve(null);
            });
        }

        return axios({
            method: options.method,
            url: url,
            data: props,
            params: options.params
        }).then(res => {
            if (res && res.data) {
                this.props = {...this.props, ...res.data};
            }
            return this;
        });
    }
}