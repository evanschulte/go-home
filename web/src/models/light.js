import Model from './model';
import axios from "axios";
import _ from "lodash"

class Light extends Model {
    url() {
        return 'api/lights/' + this.id();
    }
}

let helpers = {
    fetchAll() {
        return axios({
            method: 'get',
            url: 'api/lights'
        }).then(res => {
            let lights = [];
            if (res && res.data) {
                _.forEach(res.data, function (lt) {
                    lights.push(new Light(lt));
                });
            }

            return lights
        });
    }
};

export {
    Light,
    helpers
};