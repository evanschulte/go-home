import Model from './model';
import _ from 'lodash'

class Socket extends Model {
    constructor() {
        super();

        this.listeners = {
            read: [],
            write: []
        };

        this.open();
    }

    url() {
        return 'wss://' + window.location.host + '/connect';
    }

    open() {
        this.server = new WebSocket(this.url());
        this.server.addEventListener('open', _.bind(this.onOpen, this));
        this.server.addEventListener('message', _.bind(this.onRead, this));
        this.server.addEventListener('close', _.bind(this.onClose, this));
    }

    onOpen(e) {
        // runs after websocket connection opens
    }

    onClose(e) {
        // try to reconnect in 5 seconds
        //websocket shouldn't disconnect once initialized unless explicitly closed
        setTimeout(_.bind(function () {
            this.open();
        }, this), 5000);
    }

    onRead(e) {
        let message = JSON.parse(e.data);
        if (message) {
            this.emit(message.event, message.data);
        }
    }

    write(data) {
        // TODO: status checks
        this.server.send(data);
    }

    listenTo(event, callback) {
        if (event) {
            if (!this.listeners[event]) {
                this.listeners[event] = [];
            }
            this.listeners[event].push(callback);
        }
    }

    emit(event, data) {
        console.log(event, data);
        _.each(this.listeners[event], function (callback) {
            callback(data);
        });
    }

    close() {
        this.server.removeEventListener('open', this.onOpen);
        this.server.removeEventListener('message', this.onRead);
        this.server.removeEventListener('close', this.onClose);
    }
}

const socket = new Socket();
export {
    socket
};