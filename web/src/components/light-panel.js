import React from "react";
import {helpers} from '../models/light';
import "./light-control.scss";
import LightControl from "./light-control";

export default class LightPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lights: []
        };
    }

    componentDidMount() {
        helpers.fetchAll().then(lights => {
            this.props.lights = lights;
            this.setState({ lights });
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="col-4 text-align-center">
                    {this.state.lights.map((light, i) => {
                        return (<LightControl light={light}/>)
                    })}
                </div>
            </div>
        );
    }
}