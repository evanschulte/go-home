import React from "react";
import "./light-control.scss";
import {Light} from "../models/light";
import {socket} from "../models/websocket";

class Timer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            end: props.end ? props.end : ((new Date()).getTime() - 1)
        };
    }

    componentDidMount() {
        this.interval = setInterval(_.bind(function () {
            this.setState({state: this.state});
        }, this), 1000);
    }

    componentWillUnmount() {
        if (this.interval != null) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    render() {
        let remaining = this.props.end - (new Date).getTime();
        if (remaining <= 0) {
            remaining = 0;
        }
        return <div className="timer-count">{remaining > 0 ? (Math.ceil(remaining / 1000) + "s") : ""}</div>;
    }
}

export default class LightControl extends React.Component {
    constructor(props) {
        super(props);

        let light;
        if (!props.light || !(props.light instanceof Light)) {
            light = new Light(props.light);
        } else {
            light = props.light;
        }
        this.state = {
            light: light
        };

        socket.listenTo('light:update', _.bind(function (data) {
            if (data) {
                props.light.mergeProps(data);
                this.setState({
                    light: props.light
                });
            }
        }, this));
    }

    componentDidMount() {
        this.state.light.fetch().then(light => {
            this.setState({
                light,
                error: null
            });
        }, error => {
            this.setState({
                error
            });
        })
    }

    changeOverride() {
        this.state.light.patch({
            override: !this.state.light.props.override,
            status: this.state.light.status
        }).then(light => {
            this.setState({light});
        });
    }

    changeStatus() {
        this.state.light.patch({
            status: !this.state.light.props.status,
            override: true
        }).then(light => {
            this.setState({light});
        });
    }

    render() {
        if (!this.state.light.isFetched()) {
            return <div className="loader"></div>;
        } else {
            return (
                <div className="card">
                    <div className="card-body pt-2">
                        <div className="row no-gutters mb-2">
                            <div className="col-3 d-flex align-items-center">
                                <div className="pretty p-switch p-fill light-switch">
                                    <input type="checkbox" onChange={(e) => {
                                        this.changeOverride(e)
                                    }} checked={this.state.light.props.override}/>
                                    <div className="state p-primary">
                                        <label>&nbsp;</label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 fs-200 font-weight-light text-center">
                                Garage
                            </div>
                        </div>
                        <div className="text-center">
                            <div className="pretty p-switch p-fill light-switch">
                                <input type="checkbox" onChange={(e) => {
                                    this.changeStatus(e)
                                }} checked={this.state.light.props.status}/>
                                <div className="state p-success fs-200">
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                            <Timer end={this.state.light.props.timerExpiration}/>
                        </div>
                    </div>
                </div>
            );
        }
    }
}