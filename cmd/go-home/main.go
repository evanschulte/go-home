package main

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	"go-home/api"
	"go-home/database"
	"go-home/pkg/config"
	"go-home/pkg/light"
	"go-home/pkg/logging"
	_ "net/http/pprof"
	"os"
	"time"
)

func main() {
	app := cli.NewApp()
	app.Name = "go-home"
	app.Usage = "a home automation application"
	app.Flags = []cli.Flag{
		&cli.BoolFlag{Name: "debug"},
	}
	app.Authors = []*cli.Author{
		{
			Name:  "Evan Schulte",
			Email: "evan@evdev.xyz",
		},
	}
	app.Copyright = "(c) 2020 Evan Schulte"
	app.Version = "1.0.0"
	app.Compiled = time.Now()
	app.Before = func(c *cli.Context) error {
		config.Load()
		logging.Init()
		_, err := database.Init()
		if err != nil {
			return err
		}
		return nil
	}
	app.After = func(c *cli.Context) error {
		s := database.GetStore()
		err := s.Disconnect()
		if err != nil {
			return errors.New(fmt.Sprintf("failed to disconnect from mongodb - %s\n", err))
		}
		return nil
	}
	app.Commands = []*cli.Command{
		{
			Name:  "start",
			Usage: "start the server",
			Before: func(c *cli.Context) error {
				go func() {
					_ = light.InitAll()
				}()
				return nil
			},
			Action: func(c *cli.Context) error {
				return api.Start()
			},
		},
		{
			Name:  "light",
			Usage: "commands for lights",
			Subcommands: []*cli.Command{
				{
					Name:  "add",
					Usage: "add a new light",
					Flags: []cli.Flag{
						&cli.StringFlag{Name: "ip"},
						&cli.StringFlag{Name: "port"},
					},
					Action: func(c *cli.Context) error {
						log.Info().Msg("creating light")

						lt := light.Light{
							IP:   c.String("ip"),
							Port: c.Int("port"),
						}
						err := lt.Save()
						if err != nil {
							return err
						}
						log.Info().Msgf("light created: %+v", lt)
						return nil
					},
				},
				{
					Name:  "get",
					Usage: "get a light's properties",
					Flags: []cli.Flag{
						&cli.StringFlag{Name: "ip"},
						&cli.StringFlag{Name: "port"},
					},
					Action: func(c *cli.Context) error {
						lt := light.Light{
							IP:   c.String("ip"),
							Port: c.Int("port"),
						}
						err := lt.CheckState()
						if err != nil {
							return err
						}
						log.Info().Msgf("%+v\n", lt)
						return nil
					},
				},
				{
					Name:  "set",
					Usage: "set a light's properties",
					Flags: []cli.Flag{
						&cli.StringFlag{Name: "ip", Value: ""},
						&cli.StringFlag{Name: "port", Value: ""},
						&cli.StringFlag{Name: "status", Value: ""},
						&cli.Int64Flag{Name: "timer", Value: -1},
					},
					Action: func(c *cli.Context) error {
						lt := light.Light{
							IP:   c.String("ip"),
							Port: c.Int("port"),
						}
						if len(lt.IP) == 0 {
							return errors.New("ip address provided is not valid")
						} else if lt.Port == 0 {
							return errors.New("port provided is not valid")
						}

						lt.Status = c.String("status") == "true"
						timer := c.Int64("timer")
						lt.Timer = timer
						err := lt.PushState()
						if err != nil {
							return err
						}
						log.Trace().Msgf("light updated: %+v\n", lt)
						return nil
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err).Msgf("%s\n", err)
	}
}
