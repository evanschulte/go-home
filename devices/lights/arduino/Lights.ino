#define pirPin 7
#define ssrPin 2
#define pirPin2 8
#include <LiquidCrystal_I2C.h>
#include <neotimer.h>
#include <SPI.h>
#include <Ethernet.h>
#include <stdlib.h>
#include <ArduinoJson.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 11, 84);
EthernetServer server(80);
LiquidCrystal_I2C lcd(0x27, 20, 4);

// timer for motion detection - the main timer wont start until this timer is done
unsigned long preTimeout = 15000;
Neotimer motionPreTimer = Neotimer(preTimeout);

// timer for main functionality
unsigned long defaultTimeout = 60000;
Neotimer timer = Neotimer(defaultTimeout);

// timer for screen update
Neotimer screenTimer = Neotimer(1000);

bool lightsOn = true;
bool overrideMotion = false;

String serverAddress = "";
uint16_t serverPort = 80;
String notifyEndpoint = "";

void setup() {
  pinMode(ssrPin, OUTPUT);
  pinMode(pirPin, INPUT);
  pinMode(pirPin2, INPUT);

  turnLightsOff();

  lcd.begin();
  lcd.backlight();
  lcd.noBlink();
  lcd.clear();

  Ethernet.begin(mac, ip);
  server.begin();
}

void loop() {
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        char endOfHeaders[] = "\r\n\r\n";
        if (client.find("POST") && client.find(endOfHeaders)) {
          // Allocate the JSON document
          const size_t capacity = JSON_OBJECT_SIZE(3) + 120;
          DynamicJsonDocument json(capacity);

          // Parse JSON object
          DeserializationError error = deserializeJson(json, client);
          if (error) {
            badRequest(client);
            break;
          }

          // JSONArduino parses null string attributes as a string of "null"
          String sAddr = json["address"];
          if (sAddr.length() > 0 && sAddr != "null") {
            // the server is broadcasting the server address to us
            // we don't have to do anything with this request except set the server address and endpoint
            serverAddress = sAddr;

            uint16_t sp = json["port"];
            if (sp > 0) {
              serverPort = sp;
            }

            String nEp = json["endpoint"];
            notifyEndpoint = nEp;
          }
          else {
            bool override = json["override"];
            if (override) {
                overrideMotion = true;
              // the server is sending a command to override this device

              unsigned long newTime = json["timer"];
              // there is no command to set the timer to 0
              // if 0 is parsed for newTime, its assumed the timer was not set with this request
              // if a 0 timer is desired, a request with override: false in the message body should be sent
              if (newTime > 0) {
                timer.set(newTime);
                timer.start();
              }

              bool newStatus = json["status"];
              if (newStatus) {
                turnLightsOn();
              }
              else {
                turnLightsOff();
              }
            }
            else {
                //reset timer sets overridemotion to false
                resetTimer();
              if(lightsOn){
                motionPreTimer.start();
              }
            }
          }
        }
        ok(client);
      }
    }
  }

  if (timer.done() && (overrideMotion || motionPreTimer.done())) {
    resetTimer();
    turnLightsOff();

    // inform the server that the lights turned off after waiting for the timer to expire
    postUpdate();
  }
  else if (!overrideMotion) {
    if (checkZone(pirPin, ssrPin) || checkZone(pirPin2, ssrPin)) {
      if(timer.waiting()){
        timer.reset();
        // inform the server that the lights were turned on from motion
        postUpdate();
      }
      motionPreTimer.start();
      turnLightsOn();
    }
    else if (lightsOn && motionPreTimer.done() && !timer.waiting()) {
      // inform the server that main timer started due to motion
      timer.start();

      // inform the main timer has started
      postUpdate();
    }
  }

  if (screenTimer.repeat()) {
    updateScreen();
  }
}

void resetTimer() {
  motionPreTimer.reset();
  timer.reset();
  timer.set(defaultTimeout);
  overrideMotion = false;
}

bool checkZone(int rPin, int lPin) {
  int pinVal = digitalRead(rPin);
  return pinVal == HIGH;
}

void turnLightsOff() {
  if (lightsOn) {
    digitalWrite(ssrPin, LOW);
    lightsOn = false;
  }
}

void turnLightsOn() {
  if (!lightsOn) {
    digitalWrite(ssrPin, HIGH);
    lightsOn = true;
  }
}

void updateScreen() {
  lcd.setCursor(0, 1);
  if (!overrideMotion) {
    lcd.print("  DETECTING MOTION  ");
  }
  else {
    if (lightsOn) {
      lcd.print("     LIGHTS  ON     ");
    }
    else {
      lcd.print("     LIGHTS OFF     ");
    }
  }

  lcd.setCursor(0, 2);
  lcd.print("                    ");
  lcd.setCursor(0, 2);
  // add 500 to the remaining milliseconds - this will round up the integer that is displayed
  if (lightsOn) {
    if (overrideMotion) {
      lcd.print(" MOTION IN " + String((timer.getRemaining() + 500) / 1000) + " SEC");
    }
    else if (timer.waiting()) {
      lcd.print("   OFF IN " + String((timer.getRemaining() + 500) / 1000) + " SEC");
    }
  }
  else if (overrideMotion) {
    lcd.print(" MOTION IN " + String((timer.getRemaining() + 500) / 1000) + " SEC");
  }
}

void badRequest(EthernetClient client) {
  client.println("HTTP/1.1 400 Bad Request");
  client.println("Content-Type: application/json");
  client.println("Connection: close");
  client.println("Content-Length: 0");
  client.println();
  client.stop();
}

void ok(EthernetClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: application/json");
  client.println("Connection: close");

  DynamicJsonDocument json = buildJSON();
  client.print("Content-Length: ");
  client.println(measureJsonPretty(json));
  client.println();
  serializeJsonPretty(json, client);
  client.stop();
}

DynamicJsonDocument buildJSON() {
  const size_t capacity = JSON_OBJECT_SIZE(4);
  DynamicJsonDocument json(capacity);
  json["override"] = overrideMotion;
  json["status"] = lightsOn;
  json["timer"] = timer.getRemaining();
  json["endpoint"] = notifyEndpoint.c_str();
  return json;
}

void postUpdate() {
  if (serverAddress.length() == 0) {
    return;
  }

  EthernetClient client;
  client.setTimeout(10000);
  if (!client.connect(serverAddress.c_str(), serverPort)) {
    return;
  }

  client.println("POST " + notifyEndpoint + " HTTP/1.1");
  client.println("Host: " + serverAddress + ":" + serverPort);
  client.println("Content-Type: application/json");
  client.println("Connection: close");

  DynamicJsonDocument json = buildJSON();
  client.print("Content-Length: ");
  client.println(measureJsonPretty(json));
  client.println();
  serializeJsonPretty(json, client);
  client.stop();
}