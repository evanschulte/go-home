package http

import (
	"encoding/json"
	"net/http"
)

func Ok(w http.ResponseWriter, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(payload)
	if err != nil {
		http.Error(w, "500 - failed to encode response", http.StatusInternalServerError)
		return
	}
}
