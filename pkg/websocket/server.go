package websocket

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"net/http"
)

var server *Server

func WriteMessage(event string, data interface{}) {
	s := GetServer()
	// TODO: handle this error here
	// every caller doesnt need to handle the fact that the websocket server is failing to write messages
	_ = s.writeMessage(event, data)
}

type Server struct {
	Upgrader    websocket.Upgrader
	Connections map[string]*Connection
}

func GetServer() *Server {
	if server == nil {
		server = &Server{}
		server.Upgrader = websocket.Upgrader{}
		server.Connections = make(map[string]*Connection)
	}

	return server
}

// entry point for websocket connections
func (server *Server) Connect(w http.ResponseWriter, r *http.Request) {
	conn, err := server.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Trace().Err(err).Msg("failed to upgrade ws connection")
		return
	}

	c := newConnection(conn)
	server.addConnection(c)
}

func (server *Server) addConnection(c *Connection) {
	// store this connection to be able to broadcast to it later
	// we store it with an identifier so we can remove it when the connection is terminated

	server.Connections[c.Id.String()] = c
	log.Trace().Msgf("new websocket connection => %+v", c)
	c.Conn.SetCloseHandler(func(code int, text string) error {
		// remove the connection from this server's connections map when it gets disconnected
		delete(server.Connections, c.Id.String())
		log.Trace().Msgf("websocket connection disconnected => %+v", c)
		return nil
	})
	c.listen()
}

func (server *Server) writeMessage(event string, data interface{}) error {
	message := &Message{Event: event, Data: data}
	messageBytes, err := json.Marshal(&message)
	if err != nil {
		return err
	}
	for _, c := range server.Connections {
		go func(c *Connection) {
			_ = c.WriteMessageBytes(&messageBytes)
		}(c)
	}
	return nil
}
