package websocket

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"sync"
)

type Connection struct {
	Id   uuid.UUID
	Conn *websocket.Conn
	mu   sync.Mutex
}

func newConnection(conn *websocket.Conn) *Connection {
	c := &Connection{}
	c.Id = uuid.New()
	c.Conn = conn
	return c
}

func (c *Connection) listen() {
	for {
		messageType, message, err := c.Conn.ReadMessage()
		if err != nil {
			log.Trace().Err(err).Msg("error reading websocket message")
			return
		}
		log.Trace().Msgf("websocket message => type: %d - message: %s", messageType, message)
	}
}

func (c *Connection) WriteMessage(event string, data interface{}) error {
	message := &Message{Event: event, Data: data}
	messageBytes, err := json.Marshal(&message)
	if err != nil {
		return err
	}
	return c.WriteMessageBytes(&messageBytes)
}

func (c *Connection) WriteMessageBytes(data *[]byte) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	err := c.Conn.WriteMessage(websocket.TextMessage, *data)
	if err != nil {
		log.Debug().Err(err).Msgf("failed to write message to ws connection => %+v", &c)
		return err
	}
	return nil
}
