package logging

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go-home/pkg/config"
	"os"
)

func Init() {
	conf := config.Load()
	if conf.Debug {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	} else {
		log.Logger = zerolog.New(os.Stdout)
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
}
