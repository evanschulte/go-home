package json

import (
	"encoding/json"
	"fmt"
	"os"
)

func ParseFile(path string, st interface{}) error {
	file, err := os.Open(path)
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println("failed to close json file after attempted parsing")
		}
	}()

	if err != nil {
		return err
	}

	// try to parse the file data into the provided interface
	decoder := json.NewDecoder(file)
	err = decoder.Decode(st)
	if err != nil {
		return err
	}

	return nil
}
