package config

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net"
	"os"
	"path"
	"strconv"
)

type Configuration struct {
	IP         string
	Port       int
	Root       string
	WebRoot    string
	DBUser     string
	DBPassword string
	DBAddress  string
	DBPort     int
	DBName     string
	Debug      bool
}

var config *Configuration

func Load() *Configuration {
	if config != nil {
		return config
	}

	log.Info().Msg("reading config")

	root := os.Getenv("GOHOME_ROOT")
	// try to read the config file
	file, err := os.Open(path.Join(root, "configs", "config.json"))
	defer func() {
		if err := file.Close(); err != nil {
			log.Error().Msg("failed to close config file after reading")
		}
	}()

	if os.IsNotExist(err) {
		log.Fatal().Err(err).Msg("config file does not exist at set path")
		return nil
	} else if err != nil {
		log.Fatal().Err(err).Msg("unable to read config file")
		return nil
	}

	// try to parse the configuration file data into the Configuration struct
	decoder := json.NewDecoder(file)
	config = &Configuration{}
	err = decoder.Decode(config)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to parse config file JSON")
		return nil
	}
	config.Root = root
	config.WebRoot = path.Join(root, "web")

	if config.Port == 0 {
		port := os.Getenv("GOHOME_PORT")
		if port == "" {
			log.Fatal().Msg("GOHOME_PORT env variable was not defined")
			return nil
		}

		config.Port, err = strconv.Atoi(port)
		if err != nil {
			log.Fatal().Msg("GOHOME_PORT env variable is not a valid port")
			return nil
		}
	}

	if len(config.IP) == 0 {
		interfaces, err := net.Interfaces()
		if err != nil {
			log.Warn().Err(err).Msg("failed to get ip address of this server")
		}
		for _, i := range interfaces {
			addresses, err := i.Addrs()
			if err != nil {
				continue
			}

			for _, addr := range addresses {
				var ip net.IP
				switch v := addr.(type) {
				case *net.IPNet:
					ip = v.IP
				case *net.IPAddr:
					ip = v.IP
				}
				if ip != nil {
					config.IP = ip.String()
				}
			}
		}
	}

	return config
}
