package light

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"go-home/database"
	"go-home/pkg/config"
	ws "go-home/pkg/websocket"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"io"
	"net/http"
	"time"
)

type Light struct {
	ID              primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	IP              string             `bson:"ip" json:"ip"`
	Port            int                `bson:"port" json:"port"`
	Status          bool               `bson:"status" json:"status"`
	Timer           int64              `json:"timer"`
	TimerExpiration *int64             `bson:"timerExpiration" json:"timerExpiration"`
	Override        bool               `bson:"override" json:"override"`
	LastPing        *int64             `bson:"lastPing" json:"lastPing"`
}

func InitAll() error {
	lights, err := FetchAll()
	if err != nil {
		return err
	}

	// to initialize each light, we simply push a config to it
	// this will reply with the current state and we'll the sync will start
	for _, light := range lights {
		_ = light.PushConfig()
	}
	return nil
}

func Fetch(id primitive.ObjectID) (*Light, error) {
	result := Light{
		ID: id,
	}
	err := result.Fetch()
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func FetchAll() ([]Light, error) {
	client := database.GetStore()
	c := client.DB.Collection("lights")
	cur, err := c.Find(context.TODO(), bson.D{{}})
	if err != nil {
		return nil, err
	}

	var result []Light
	for cur.Next(context.TODO()) {
		var light Light
		err := cur.Decode(&light)
		if err != nil {
			return nil, err
		}

		result = append(result, light)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	err = cur.Close(context.TODO())
	if err != nil {
		log.Trace().Err(err).Msg("failed to close cursor after getting lights")
	}

	return result, nil
}

func (light *Light) getUrl() string {
	return fmt.Sprintf("http://%s:%d", light.IP, light.Port)
}

func (light *Light) fromJSON(data io.ReadCloser) error {
	err := json.NewDecoder(data).Decode(light)
	if err != nil {
		log.Trace().Err(err).Msgf("failed to parse light data => ip: %+v", light)
		return errors.New("failed to decode light response")
	}
	return nil
}

func (light *Light) stateToJson() interface{} {
	var tmp struct {
		Status   bool  `json:"status"`
		Timer    int64 `json:"timer"`
		Override bool  `json:"override"`
	}
	tmp.Status = light.Status
	tmp.Timer = light.Timer
	tmp.Override = light.Override
	return tmp
}

func (light *Light) configToJson() interface{} {
	var tmp struct {
		Address  string `json:"address"`
		Port     int    `json:"port"`
		Endpoint string `json:"endpoint"`
	}
	conf := config.Load()
	tmp.Address = conf.IP
	tmp.Port = conf.Port
	tmp.Endpoint = "/api/lights/" + light.ID.Hex() + "/notify"
	return tmp
}

func (light *Light) calculateTimer() {
	u := time.Now().UnixNano() / int64(time.Millisecond)
	light.LastPing = &u
	if light.Timer > 0 {
		exp := u + light.Timer
		light.TimerExpiration = &exp
	} else {
		light.TimerExpiration = nil
	}
}

func (light *Light) CheckState() error {
	r, err := http.Get(light.getUrl())
	if err != nil || r.StatusCode != 200 {
		log.Trace().Err(err).Msgf("failed to fetch light state => %v", light)
		return errors.New("failed to fetch light state")
	}

	err = light.fromJSON(r.Body)
	if err != nil {
		return err
	}

	log.Trace().Msgf("checked light state => %+v", light)
	return light.Save()
}

func (light *Light) PushState() error {
	data := light.stateToJson()
	err := light.push(data)
	if err != nil {
		log.Trace().Err(err).Msgf("failed to push state to light => data:%+v", data)
		return err
	}

	log.Trace().Msgf("updated light state => %+v", data)
	return nil
}

func (light *Light) PushConfig() error {
	data := light.configToJson()
	err := light.push(data)
	if err != nil {
		log.Trace().Err(err).Msgf("failed to push config to light => data:%v", data)
		return err
	}

	log.Trace().Msgf("updated light config => %+v", data)
	return nil
}

func (light *Light) push(data interface{}) error {
	// marshall the data to be sent
	js, err := json.Marshal(&data)
	if err != nil {
		log.Trace().Err(err).Msgf("failed to serialize data for light => %+v", data)
		return err
	}

	// push the data to the light
	r, err := http.Post(light.getUrl(), "application/json", bytes.NewBuffer(js))
	if err != nil {
		return err
	} else if r.StatusCode != 200 {
		return errors.New(fmt.Sprintf("light returned non-ok status code %d", r.StatusCode))
	}

	// parse the response from the light
	// the response will always be the current state of the light
	err = light.fromJSON(r.Body)
	if err != nil {
		return err
	}
	return light.Save()
}

func (light *Light) Fetch() error {
	client := database.GetStore()
	c := client.DB.Collection("lights")
	err := c.FindOne(context.TODO(), bson.M{"_id": light.ID}).Decode(&light)
	if err != nil {
		log.Trace().Err(err).Msg("failed to fetch light from database")
	}
	return err
}

func (light *Light) Save() error {
	// calculate the timer
	light.calculateTimer()

	client := database.GetStore()
	c := client.DB.Collection("lights")

	if light.Timer == 0 {
		light.TimerExpiration = nil
	}

	if light.ID.IsZero() {
		id := primitive.NewObjectID()
		light.ID = id
		_, err := c.InsertOne(context.TODO(), &light)
		if err != nil {
			return err
		}

		if light.LastPing == nil || *light.LastPing <= 0 {
			go func(light *Light) {
				err = light.CheckState()
				if err == nil {
					_ = light.PushConfig()
				}
			}(light)
		}
		ws.WriteMessage("light:add", light)
	} else {
		filter := bson.M{"_id": bson.M{"$eq": light.ID}}
		_, err := c.ReplaceOne(context.TODO(), filter, light)
		if err != nil {
			log.Trace().Err(err).Msg("failed to save light to database")
			return err
		}
		ws.WriteMessage("light:update", light)
	}

	return nil
}

func (light *Light) Notified(status bool, timer int64, override bool) error {
	light.Status = status
	light.Timer = timer
	light.Override = override
	err := light.Save()
	if err != nil {
		return err
	}

	go func(light *Light) {
		ws.WriteMessage("light:update", light)
	}(light)

	return nil
}

func CreateIndexes() ([]string, error) {
	client := database.GetStore()
	c := client.DB.Collection("lights")
	index := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "ip", Value: bsonx.String("text")}},
		},
	}
	opts := options.CreateIndexes().SetMaxTime(10 * time.Second)
	return c.Indexes().CreateMany(context.TODO(), index, opts)
}
