module go-home

go 1.14

require (
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/rs/zerolog v1.18.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/urfave/cli/v2 v2.2.0
	go.mongodb.org/mongo-driver v1.3.3
)
