# build golang project
FROM golang:alpine AS golang
WORKDIR /go/src/go-home
COPY go.mod go.sum ./
RUN go mod download
COPY /cmd ./cmd
COPY /database ./database
COPY /pkg ./pkg
COPY /api ./api
RUN go install ./cmd/go-home

# build webpack/react project
FROM node:12.19.1-stretch-slim AS node
WORKDIR /build
COPY .babelrc webpack.config.js package.json package-lock.json ./
RUN npm install
COPY ./web ./web
RUN npm run build

# create a new image with the builds from the golang/webpack images
FROM alpine:latest as go-home
LABEL maintainer="Evan Schulte <evan@evdev.xyz>"
WORKDIR /gohome
COPY --from=golang /go/bin .
COPY --from=node /build/web/dist ./web
COPY /configs/config.debug.json ./configs/config.json
ENV GOHOME_ROOT=/gohome
ENV GOHOME_PORT=80
EXPOSE 80
ENTRYPOINT ["./go-home", "start"]